<?php
$loginModule = Page::getInstance()->getModule("login");
$sessionUserId = $loginModule->isLogged() ? $loginModule->getUserId() : -1;

echo <<<EOF
<div class="blogSectionsLeft"> </div>
<div class="content">
EOF;

$items = $thisModule->loadBlogPost($_GET["id"]);
if($items["visible"] == 0 && !($sessionUserId == $items["userId"])) {
    echo "Wrong blog post id!";
    redirect("blog", 3);
    exit;
}

echo <<<EOF
<div class="blogPostTitle"> $items[title] </div>
<div class="blogPostAuthor"> Posted by: $items[author] </div>
<div class="blogPostDate"> Posted on date: $items[formattedDate] </div>
<div class="blogPostTags"> Tags: $items[tags]</div>
EOF;

/* Add edit button only if this is the right author by user id. */
$editLink = Constants::$webPath . "/blog/edit?id=$_GET[id]";
if($items["visible"]) {
    $visibleValue = "<span class=\"blogPostVisibleTrue\"> True </span>";
}
else {
    $visibleValue = "<span class=\"blogPostVisibleFalse\"> False </span>";
}

if($sessionUserId == $items["userId"]) {
    echo <<<EOF
    <div class="blogPostEdit"> <a href="$editLink"> Edit post </a> </div>
    <div class="blogPostVisible">
        <form method="post">
            <button class="blogToggleVisibility" name="submitToggleVisibility">
                Visibility: $visibleValue
            </button>
        </form>
    </div>
    EOF;
}

echo <<<EOF
<div class="blogContentPage"> $items[content] </div>
EOF;

$page->getFooter();
?>