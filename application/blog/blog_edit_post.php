<?php
$page = Page::getInstance("Blog post", ["login", "blog"]);
$thisModule = $page->getModule("blog");
$loginModule = $page->getModule("login");
$sessionUserId = $loginModule->isLogged() ? $loginModule->getUserId() : -1;

if(!$loginModule->isLogged()) {
	redirectWithMessage("blog", 3, "You need to be logged in to add blog posts.");
	exit;
}

if(!isset($_GET["id"])) {
	redirectWithMessage("blog", 3, "No blog post id was provided.");
	exit;
}

$blogPostUserId = $thisModule->getBlogPostFromDB($_GET["id"])["userId"];
if($sessionUserId != $blogPostUserId) {
	redirectWithMessage("blog", 3, "You do not have permissions to update this blog post.");
	exit;
}

if(isset($_POST["submitButton"])) {
    $thisModule->updateBlogPost($_GET["id"], $_POST["blogTitle"], $_POST["blogTags"], $_POST["blogFileContent"]);
	redirectWithMessage("blog?id=$_GET[id]", 3, "Blog post was edited successfully!");
	exit;
}

if(isset($_POST["saveButton"])) {
	$thisModule->updateBlogPost($_GET["id"], $_POST["blogTitle"], $_POST["blogTags"], $_POST["blogFileContent"]);
	refresh();
	exit;
}

$page->getHeader();
$blogPost = $thisModule->getBlogPostFromDB($_GET["id"]);
$date = date("Y-m-d", $blogPost["date"]);
echo <<<EOF
<div id="blogAuthor" style="display:none">$blogPost[author]</div>
<div id="blogDate" style="display:none">$date</div>
<div class="blogPostAddLeft">
	<form method="post">
		<div class="left">
			Title <input type="text" value="$blogPost[title]" name="blogTitle" class="blogTitle"> <br/>
			Tags (delimited by commas) <input type="text" value="$blogPost[tags]" name="blogTags" class="blogTags"> <br/>
		</div>
		<div class="right">
			<button name="saveButton" class="liveEditSaveButton"> Save </button>
			<span id="liveEditSavedMessage"></span>
		</div>
		Content </br>
		<textarea class="blogTextArea" name="blogFileContent">$blogPost[content]</textarea> <br/>
	</form>
</div>
<div class="blogPostAddRight"> </div>
EOF;

$page->getFooter();
?>