<?php
$page = Page::getInstance("CMS :: Blog", ["main_menu", "blog", "login"]);
$page->getHeader();
include_once(Constants::$applicationPath . "/menu.php");
$thisModule = $page->getModule("blog");

if(isset($_POST["submitToggleVisibility"])) {
	assert (isset($_GET["id"]));
	$thisModule->toggleVisibility($_GET["id"]);
	refresh();
}

if(isset($_GET["id"])) {
	require_once("blog_post.php");
	exit;
}

require_once("blog_list.php");
