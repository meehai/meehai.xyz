<?php

$page = Page::getInstance("Blog post", ["login", "blog"]);
$thisModule = $page->getModule("blog");
$loginModule = $page->getModule("login");
$loginModule->enableWidget = false;

if(!$loginModule->isLogged()) {
	redirectWithMessage("blog", 3, "You need to be logged in to add blog posts.");
	exit;
}

$page->getHeader();

if(isset($_POST["submitButton"])) {
	$id = $thisModule->addBlogPost($_POST["blogTitle"], $_POST["blogTags"],
		$_POST["blogFileContent"], $_SESSION["user"] );
	if(!$id) {
		echo "Error adding blog post $_POST[blogTitle]";
	}
	redirect("blog?id=$id", 3);
	echo "Blog post added successfully!";

	exit;
}

echo <<<EOF
<div class="blogPostAddLeft">
	<form method="post">
		<div class="left">
			Title <input type="text" value="" name="blogTitle" class="blogTitle"> <br/>
			Tags (delimited by commas) <input type="text" value=""
				name="blogTags" class="blogTags"> <br/>
		</div>
		<div class="right">
			<button name="submitButton"> Create! </button>
		</div>
		Content </br>
		<textarea class="blogTextArea" name="blogFileContent"></textarea> <br/>
	</form>
</div>
<div class="blogPostAddRight"> </div>
EOF;

$page->getFooter();
