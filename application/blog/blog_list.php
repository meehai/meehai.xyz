<?php

echo <<<EOF
<div class="content" style="text-align:center">
	<div class="blogTopMessage"> Blog posts </div>
EOF;


function getVisibleBlogPosts($thisModule) {
	$loginModule = Page::getInstance()->getModule("login");
	$sessionUserId = $loginModule->isLogged() ? $loginModule->getUserId() : -1;
	$blogPosts = $thisModule->getBlogPosts();
	$visibleBlogPosts = [];
	# Go in reverse order so the last entry is the first shown.
	for ($i=count($blogPosts) - 1; $i>=0; $i--) {
		if($blogPosts[$i]["visible"] == 0 && $sessionUserId != $blogPosts[$i]["userId"]) {
			continue;
		}
		$visibleBlogPosts[] = $blogPosts[$i];
	}
	return $visibleBlogPosts;
}

function getPage() {
	$page = 0;
	if (isset($_GET["page"])) {
		$page = $_GET["page"] - 1;
	}
	assert ($page >= 0);
	return $page;
}

function displayBlogPosts($visibleBlogPosts, $nShown) {
	$loginModule = Page::getInstance()->getModule("login");
	$sessionUserId = $loginModule->isLogged() ? $loginModule->getUserId() : -1;
	$page = getPage();
	$numPages = floor(count($visibleBlogPosts) / $nShown) + (count($visibleBlogPosts) % $nShown != 0) - 1;
	if ($numPages < 0) {
		return;
	}

	$page = min($page, $numPages);
	$startIx = $page * $nShown;
	$endIx = min(count($visibleBlogPosts), ($page + 1) * $nShown);
	for($i=$startIx; $i<$endIx; $i++) {
		$blogPost = $visibleBlogPosts[$i];
		$blogPostLink = Constants::$webPath."/blog?id=$blogPost[id]";
		$author = $blogPost["author"];
		$visibleMessage = "";
		if($sessionUserId == $blogPost["userId"]) {
			$author .= " (you)";
			$visibleMessage = "Visible: ";
			if($blogPost["visible"]) {
				$visibleMessage .= "<span class=\"blogPostVisibleTrue\"> True </span>";
			}
			else {
				$visibleMessage .= "<span class=\"blogPostVisibleFalse\"> False </span>";
			}
		}
		$content = $blogPost["content"];
		$content = strip_tags($content);
		$content = substr($content, 0, 100);

		echo <<<EOF
		<a class="blogListLinkItem" href=$blogPostLink>
			<div class="blogListItem">
				<div class="blogListItemTitle"> $blogPost[title] </div>
				<div> Posted by <span class="blogListItemAuthor">$author</span> on date <span class="blogListItemDate"> $blogPost[formattedDate] </span> </div>
				<div class="blogListItemTags"> Tags: $blogPost[tags] </div>
				<div class="blogListItemTags"> $visibleMessage </div>
				<div class="blogListItemContentPreview"> $content ... </div>
				<div class="blogListItemContentReadMore"> Read more! </div>
			</div>
		</a>
		EOF;
	}
	if ($page != 1) {
		$pagesLink = ["1" => Constants::$webPath."/blog?page=1"];
	}
	if ($page != 0) {
		$pagesLink["<"] = Constants::$webPath."/blog?page=".($page);
	}
	$pagesLink[($page + 1)] = Constants::$webPath."/blog?page=".($page + 1);
	if ($page < $numPages - 1) {
		$pagesLink[">"] = Constants::$webPath."/blog?page=".($page+2);
	}
	$pagesLink[$numPages + 1] = Constants::$webPath."/blog?page=".($numPages + 1);
	
	
	echo "Pages: ";
	foreach($pagesLink as $k=>$v){
		echo <<<EOF
		<a href=$v class="blogListLinkItem"> $k </a>
		EOF;
	}
	
}

$visibleBlogPosts = getVisibleBlogPosts($thisModule);

if(count($visibleBlogPosts) == 0) {
	echo <<<EOF
	<h3> Empty blog is empty :( </h3>
	EOF;
}
else {
	$nShown = 10;
	displayBlogPosts($visibleBlogPosts, $nShown);	
}

echo <<<EOF
	</div>
</div>
EOF;