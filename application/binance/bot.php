<?php
$page = Page::getInstance("MeehaiXYZ :: Binance :: Bot", ["main_menu", "login"]);
$page->getHeader();
$loginModule = $page->getModule("login");
include_once(Constants::$applicationPath . "/menu.php");

$botPngPath = Constants::$webPath . "/bot.png";

echo <<<EOF
<div class="content">
    <h3> Bot evolution so we think we're making money. <br/>In reality we lose and have no control over it. </h3> <br/>
    <img src="$botPngPath" id="mainFig"> 
</div>

<script type="text/javascript">
    function updateImage() {
        document.getElementById("mainFig").src = "$botPngPath?" + new Date().getTime();
    }

    updateImage();
    window.setInterval(updateImage, 500);
</script>
EOF;

$page->getFooter();



