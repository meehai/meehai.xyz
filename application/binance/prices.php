<?php
$page = Page::getInstance("MeehaiXYZ :: Binance :: Prices", ["main_menu", "login"]);
$page->getHeader();
$loginModule = $page->getModule("login");
include_once(Constants::$applicationPath . "/menu.php");

$balancePath = Constants::$webPath . "/prices.png";

echo <<<EOF
<div class="content">
    <h3> Price tracker so we buy automatically when we think it goes up and lose money fast.</h3> <br/>
    <img src="$balancePath" id="mainFig"> 
</div>

<script type="text/javascript">
    function updateImage() {
        document.getElementById("mainFig").src = "$balancePath?" + new Date().getTime();
    }

    updateImage();
    window.setInterval(updateImage, 500);
</script>
EOF;

$page->getFooter();



