<?php
$page = Page::getInstance("MeehaiXYZ :: Binance", ["main_menu", "login"]);
$page->getHeader();
include_once(Constants::$applicationPath . "/menu.php");

$prices = Constants::$webPath . "/binance/prices";
$balance = Constants::$webPath . "/binance/balance";
$bot = Constants::$webPath . "/binance/bot";
echo <<<EOF
<div class="content">
	Price tracker: <a href="$prices"> link </a> <br/>
	Balance: <a href="$balance"> link </a> <br/>
	Bot: <a href="$bot"> link </a>
</div>
EOF;

$page->getFooter();
