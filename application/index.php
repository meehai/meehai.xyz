<?php
$page = Page::getInstance("CMS :: Index", ["main_menu", "login"]);
$page->getHeader();
$loginModule = $page->getModule("login");
include_once("menu.php");

echo <<<EOF
<div class="content">
	Hello, world! <br/>
EOF;

if($loginModule->isLogged()) {
	$loggedTime = $loginModule->getTimeSinceLogin();
	echo "Time logged: " . $loggedTime . "s. Max logged time: " . $loginModule::$maxLoggedTime . "s<br/>";
}

echo "<br/>";
Constants::printConstants();

echo "</div>";
$page->getFooter();
