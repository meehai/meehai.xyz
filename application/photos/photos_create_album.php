<?php
$page = Page::getInstance("Photos", ["login", "photos"]);
$thisModule = $page->getModule("photos");
$loginModule = $page->getModule("login");

if(!$loginModule->isLogged()) {
	redirectWithMessage("photos", 3, "You need to be logged in to create albums.");
	exit;
}

$page->getHeader();
?>

<?php
if(isset($_POST["submitButton"])) {
    $id = $thisModule->createAlbum($_POST["albumTitle"], $_SESSION["user"]);
    if(!$id) {
		echo "Error adding blog post $_POST[albumTitle]";
	}

    redirect("photos?id=$id", 3);
	echo "Album created successfully!";

	exit;
}
?>

<div class="content">
    <form method="post">
        Album Title <input type="text" name="albumTitle" class="albumTitle"> <br/>
        <input type="submit" name="submitButton" value="Submit">
    </form>
</div>

<?php
$page->getFooter();
?>