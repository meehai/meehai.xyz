<?php
$page = Page::getInstance("CMS :: Photos", ["main_menu", "login", "photos"]);
$page->getHeader();
include_once(Constants::$applicationPath . "/menu.php");
$thisModule = $page->getModule("photos");

if(!isset($_GET["id"])) {
    redirect("photos");
}

$album = $thisModule->loadAlbum($_GET["id"]);
if(!$album) {
    echo "Wrong album id!";
    redirect("photos", 3);
    exit;
}

$webPath = Constants::$webPath . "/public_files/photos/{$album->albumId}";
$back = Constants::$webPath . "/photos";

echo <<<EOF
<div class="content">
<a href="$back"> < </a>
<h1>{$album->albumTitle}</h1>
Posted by {$album->authorName}
EOF;

$photos = $album->getAlbumPhotos();
foreach ($photos as $photo) {
    $photoPath = Constants::$webPath . "/photos/photo?albumId={$album->albumId}&photoId=$photo[id]";
    echo <<<EOF
        <a href="$photoPath">
            <div>
                <img src="$webPath/$photo[thumbnailFile]">
            <div/>
        </a>
    EOF;
}

echo "</div>"

?>


<?php
$page->getFooter();
?>