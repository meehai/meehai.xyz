if [ $# -ne 1 ]; then
	echo "Usage: ./install <valmih_CMS_path>";
	exit 1;
fi

vmPath=`realpath $1`;
echo "Installing to $vmPath";

if [ ${vmPath: -1} == "/" ]; then
	vmPath=${vmPath:0:-1};
fi

function msgExit {
	echo $1;
	exit 1;
}

function wrongVMPath {
	echo "Wrong ValMih CMS path directory";
	echo "Recommended command: 'git clone https://gitlab.com/valmih-cms/valmih-CMS $1' and follow web server instalation steps from README";
	exit 1;
}

[ -d $vmPath ] || wrongVMPath $vmPath;
[ -d "$vmPath/core" ] || wrongVMPath $vmPath;
[ -d "$vmPath/modules" ] || wrongVMPath $vmPath;
[ -d "$vmPath/themes" ] || wrongVMPath $vmPath;

# Update submodules
pwd=`pwd`

echo "Application"
rm -rf $vmPath/application
touch $vmPath/.keep
ln -s $pwd/application $vmPath/application

echo "Theme"
rm -rf $vmPath/themes/MeehaiXYZ
ln -s $pwd/themes/MeehaiXYZ $vmPath/themes/MeehaiXYZ 


# Database
echo "DB";
if [ ! -d "$vmPath/db" ]; then
	git clone git@gitlab.com:mihaicristianpirvu/meehai.xyz-db.git $vmPath/db
fi
cd $vmPath/db && git fetch && git reset --hard HEAD && git pull origin master && cd $pwd;
chmod -R 777 $vmPath/db

# Modules
moduleNames=("blog" "photos" "TodoList")
moduleLinks=("https://gitlab.com/valmih-cms/modules/blog.git" "https://gitlab.com/valmih-cms/modules/photos.git" "https://gitlab.com/valmih-cms/modules/Todo-List.git")

for i in ${!moduleNames[@]}; do
	name=${moduleNames[$i]}
	link=${moduleLinks[$i]}
	moduleDir="$vmPath/modules/$name"
	echo "Module $name. Link: $link. Dir: $moduleDir. ";
	if [ ! -d "$moduleDir" ]; then
		git clone $link $moduleDir;
		chmod 777 $moduleDir;
	fi
	cd $moduleDir && git fetch && git reset --hard HEAD && git pull origin master && cd $pwd;
done

